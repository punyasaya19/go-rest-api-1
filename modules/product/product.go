package product

import (
	"time"

	"gitlab.com/punyasaya19/go-api-2/modules/category"
)

type Product struct {
	ID          int64   `json:"id" gorm:"primaryKey;auto_increment:true;index"`
	Name        string  `json:"name" gorm:"varchar(255)"`
	Stock       int64   `json:"stock" gorm:"int(11)"`
	Description string  `json:"description" gorm:"text"`
	Image       *string `json:"image" gorm:"varchar(255)"`
	CategoryID  int64   `json:"category_id"`
	Category    category.Category
	CreatedAT   time.Time `json:"created_at"`
	UpdatedAT   time.Time `json:"updated_at"`
}
