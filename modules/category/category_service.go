package category

import (
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type CategoryServiceImpl struct {
	categoryRepository CategoryRepository
}

// Interface
type CategoryService interface {
	GetAll() []Category
	GetById(id int64) Category
	Create(categoryCreateInput CategoryCreateInput, ctx *gin.Context) (category *Category, statusCode int, err error)
	Update(id int64, categoryUpdateInput CategoryUpdateInput, ctx *gin.Context) (category *Category, statusCode int, err error)
	Delete(id int64, ctx *gin.Context) (category *Category, statusCode int, err error)
}

func (service *CategoryServiceImpl) GetAll() []Category {
	return service.categoryRepository.FindAll()
}

func (service *CategoryServiceImpl) GetById(id int64) Category {
	return service.categoryRepository.FindById(id)
}

func (service *CategoryServiceImpl) Create(categoryCreateInput CategoryCreateInput, ctx *gin.Context) (category *Category, statusCode int, err error) {
	// Check Name Exist Or Not
	cekCategoryByName := service.categoryRepository.FindByName(categoryCreateInput.Name)
	if cekCategoryByName == 1 {
		// Jika Sudah Ada
		return nil, http.StatusBadRequest, errors.New("name Has Alredy Exist")
	}

	// Prepare Category input data
	categoryData := Category{
		Name:      categoryCreateInput.Name,
		CreatedAT: time.Now(),
		UpdatedAT: time.Now(),
	}

	// Insert
	result, err := service.categoryRepository.Create(categoryData)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return result, http.StatusOK, nil
}

func (service *CategoryServiceImpl) Update(id int64, categoryUpdateInput CategoryUpdateInput, ctx *gin.Context) (category *Category, statusCode int, err error) {
	// Prepare Category Update Data
	categoryData := Category{
		ID:        id,
		Name:      categoryUpdateInput.Name,
		UpdatedAT: time.Now(),
	}

	// Update
	result, err := service.categoryRepository.Update(categoryData)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return result, http.StatusOK, nil
}

func (service *CategoryServiceImpl) Delete(id int64, ctx *gin.Context) (category *Category, statusCode int, err error) {
	// Prepare Category Update Data
	categoryData := Category{
		ID: id,
	}

	// Update
	result, err := service.categoryRepository.Delete(categoryData)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return result, http.StatusOK, nil
}

// Init Service
func NewCategoryService(con *gorm.DB) CategoryService {
	return &CategoryServiceImpl{categoryRepository: NewCategoryRepository(con)}
}
