package category

type CategoryCreateInput struct {
	Name string `json:"name" validate:"required"`
}

type CategoryUpdateInput struct {
	Name string `json:"name" validate:"required"`
}
