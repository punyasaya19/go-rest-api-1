package category

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/punyasaya19/go-api-2/helper"
	"gorm.io/gorm"
)

type CategoryController struct {
	categoryService CategoryService
}

// init controller
func NewCategoryController(con *gorm.DB) CategoryController {
	return CategoryController{
		categoryService: NewCategoryService(con),
	}
}

func (controller *CategoryController) Index(ctx *gin.Context) {

	result := controller.categoryService.GetAll()

	ctx.JSON(http.StatusOK, helper.ResponseJson{
		Status:  true,
		Message: "Success Get All Data",
		Data:    result,
		Error:   nil,
	})
}

func (controller *CategoryController) Show(ctx *gin.Context) {
	// Get id Request
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Enter Valid ID",
			Data:    nil,
			Error:   err,
		})
		return
	}

	result := controller.categoryService.GetById(int64(reqId))
	ctx.JSON(http.StatusOK, helper.ResponseJson{
		Status:  true,
		Message: "Success Get Data By ID",
		Data:    result,
		Error:   nil,
	})
}

func (controller *CategoryController) Create(ctx *gin.Context) {
	// Parse json req
	var categoryInput CategoryCreateInput
	err := ctx.ShouldBind(&categoryInput)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Failed Parseing JSON Request",
			Data:    nil,
			Error:   err,
		})
		return
	}

	// Validation input
	validate := validator.New()
	err = validate.Struct(&categoryInput)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Validation Error",
			Data:    nil,
			Error:   err.(validator.ValidationErrors),
		})
		return
	}

	// create
	result, statusCode, err := controller.categoryService.Create(categoryInput, ctx)
	if err != nil {
		fmt.Println("Errror ", err)
		ctx.JSON(statusCode, helper.ResponseJson{
			Status:  false,
			Message: "Failed To Create New Category",
			Data:    nil,
			Error:   err,
		})
		return
	}

	ctx.JSON(statusCode, helper.ResponseJson{
		Status:  true,
		Message: "Success Create New Category",
		Data:    result,
		Error:   nil,
	})
}

func (controller *CategoryController) Update(ctx *gin.Context) {
	// Get id Request
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Enter Valid ID",
			Data:    nil,
			Error:   err,
		})
		return
	}

	// Parse json req
	var categoryInput CategoryUpdateInput
	err = ctx.ShouldBind(&categoryInput)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Failed Parseing JSON Request",
			Data:    nil,
			Error:   err,
		})
		return
	}

	// Validation input
	validate := validator.New()
	err = validate.Struct(&categoryInput)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Validation Error",
			Data:    nil,
			Error:   errors.New(err.Error()),
		})
		return
	}

	// Update
	result, statusCode, err := controller.categoryService.Update(int64(reqId), categoryInput, ctx)
	if err != nil {
		ctx.JSON(statusCode, helper.ResponseJson{
			Status:  false,
			Message: "Failed To Update Category",
			Data:    nil,
			Error:   err,
		})
		return
	}

	ctx.JSON(statusCode, helper.ResponseJson{
		Status:  true,
		Message: "Success Update Category",
		Data:    result,
		Error:   nil,
	})
}

func (controller *CategoryController) Delete(ctx *gin.Context) {
	// Get id Request
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Enter Valid ID",
			Data:    nil,
			Error:   err,
		})
		return
	}

	// Delete
	result, statusCode, err := controller.categoryService.Delete(int64(reqId), ctx)
	if err != nil {
		ctx.JSON(statusCode, helper.ResponseJson{
			Status:  false,
			Message: "Failed To Delete Category",
			Data:    nil,
			Error:   err,
		})
		return
	}

	ctx.JSON(statusCode, helper.ResponseJson{
		Status:  true,
		Message: "Success Delete Category",
		Data:    result,
		Error:   nil,
	})
}
