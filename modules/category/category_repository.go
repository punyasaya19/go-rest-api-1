package category

import (
	"gorm.io/gorm"
)

type CategoryRepositoryImpl struct {
	db *gorm.DB
}

// Interface
type CategoryRepository interface {
	FindAll() []Category
	FindById(id int64) Category
	FindByName(name string) int64
	Create(category Category) (*Category, error)
	Update(category Category) (*Category, error)
	Delete(category Category) (*Category, error)
}

func (repo *CategoryRepositoryImpl) FindAll() []Category {
	var categories []Category

	repo.db.Find(&categories)

	return categories
}
func (repo *CategoryRepositoryImpl) FindById(id int64) Category {
	var category Category

	repo.db.First(&category, id)

	return category
}
func (repo *CategoryRepositoryImpl) FindByName(name string) int64 {
	var category Category

	result := repo.db.First(&category, "name = ?", name)
	return result.RowsAffected
}
func (repo *CategoryRepositoryImpl) Create(category Category) (*Category, error) {
	result := repo.db.Create(&category)
	if result.Error != nil {
		return nil, result.Error
	}
	return &category, nil
}
func (repo *CategoryRepositoryImpl) Update(category Category) (*Category, error) {
	// result := repo.db.Save(&category)
	result := repo.db.Model(&category).Select("name", "updated_at").Updates(&category)
	if result.Error != nil {
		return nil, result.Error
	}
	return &category, nil
}
func (repo *CategoryRepositoryImpl) Delete(category Category) (*Category, error) {
	result := repo.db.Delete(&category)
	if result.Error != nil {
		return nil, result.Error
	}
	return &category, nil
}

// Func Init Repo
func NewCategoryRepository(con *gorm.DB) CategoryRepository {
	return &CategoryRepositoryImpl{db: con}
}
