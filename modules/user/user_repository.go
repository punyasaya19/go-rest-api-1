package user

import "gorm.io/gorm"

type UserRepositoryImpl struct {
	db *gorm.DB
}

// Create implements UserRepository.
func (ur *UserRepositoryImpl) Create(user User) (*User, error) {
	result := ur.db.Create(&user)

	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

// Delete implements UserRepository.
func (ur *UserRepositoryImpl) Delete(user User) (*User, error) {
	result := ur.db.Delete(&user)

	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

// FindAll implements UserRepository.
func (ur *UserRepositoryImpl) FindAll() []User {
	var users []User

	_ = ur.db.Find(&users)

	return users
}

// FindById implements UserRepository.
func (ur *UserRepositoryImpl) FindById(id int) User {
	var user User

	_ = ur.db.First(&user, id)

	return user
}

// FindByUsername implements UserRepository.
func (ur *UserRepositoryImpl) FindByUsername(username string) (sts int, user User) {

	result := ur.db.Where("username = ?", username).Limit(1).Find(&user)
	if result.Error != nil {
		return 0, user
	}
	return int(result.RowsAffected), user
}

// Update implements UserRepository.
func (ur *UserRepositoryImpl) Update(user User) (*User, error) {
	result := ur.db.Save(&user)

	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

// Interface
type UserRepository interface {
	FindAll() []User
	FindById(id int) User
	FindByUsername(username string) (sts int, user User)
	Create(user User) (*User, error)
	Update(user User) (*User, error)
	Delete(user User) (*User, error)
}

// func init Repo
func NewUserRepository(con *gorm.DB) UserRepository {
	return &UserRepositoryImpl{db: con}
}
