package user

import (
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserServiceImpl struct {
	userRepository UserRepository
}

// Create implements UserService.
func (us *UserServiceImpl) Create(ctx *gin.Context) (*User, error) {
	var userCreateInput UserCreateInput

	// Bind Request to json
	err := ctx.ShouldBindJSON(&userCreateInput)
	if err != nil {
		return nil, err
	}

	// fmt.Println("Inputan Created Adalah = ", userCreateInput)

	// Ini validasi

	// Prepare data insert
	userCreate := User{
		Name:      userCreateInput.Name,
		Username:  userCreateInput.Username,
		CreatedAT: time.Now(),
		UpdatedAT: time.Now(),
	}

	// enkripsi password
	bytes, err := bcrypt.GenerateFromPassword([]byte(userCreateInput.Password), 16)
	if err != nil {
		return nil, err
	}
	userCreate.Password = string(bytes)

	// Insert Data
	result, err := us.userRepository.Create(userCreate)
	if err != nil {
		return nil, err
	}

	return result, nil
}

// Delete implements UserService.
func (us *UserServiceImpl) Delete(ctx *gin.Context) (*User, error) {
	// Get id update & convert to int
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return nil, err
	}

	user := User{
		ID: int64(reqId),
	}

	// Delete data
	result, err := us.userRepository.Delete(user)
	if err != nil {
		return nil, err
	}

	return result, nil
}

// GetAll implements UserService.
func (us *UserServiceImpl) GetAll() []User {
	return us.userRepository.FindAll()
}

// GetById implements UserService.
func (us *UserServiceImpl) GetById(id int) User {
	return us.userRepository.FindById(id)
}

// GetByUsername implements UserService.
func (us *UserServiceImpl) GetByUsername(username string) (sts int, user User) {
	return us.userRepository.FindByUsername(username)
}

// Update implements UserService.
func (us *UserServiceImpl) Update(ctx *gin.Context) (*User, error) {
	// Get id update & convert to int
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return nil, err
	}

	var userUpdateInput UserUpdateInput

	// Bind Request to json
	err = ctx.ShouldBindJSON(&userUpdateInput)
	if err != nil {
		return nil, err
	}

	// Ini validasi

	// Prepare data update
	userUpdate := User{
		ID:        int64(reqId),
		Name:      userUpdateInput.Name,
		Username:  userUpdateInput.Username,
		CreatedAT: time.Now(),
		UpdatedAT: time.Now(),
	}

	// enkripsi password
	bytes, err := bcrypt.GenerateFromPassword([]byte(userUpdateInput.Password), 16)
	if err != nil {
		return nil, err
	}
	userUpdate.Password = string(bytes)

	// update Data
	result, err := us.userRepository.Update(userUpdate)
	if err != nil {
		return nil, err
	}

	return result, nil
}

// Interface
type UserService interface {
	GetAll() []User
	GetById(id int) User
	GetByUsername(username string) (sts int, user User)
	Create(ctx *gin.Context) (*User, error)
	Update(ctx *gin.Context) (*User, error)
	Delete(ctx *gin.Context) (*User, error)
}

// Func to init
func NewUserService(con *gorm.DB) UserService {
	return &UserServiceImpl{userRepository: NewUserRepository(con)}
}
