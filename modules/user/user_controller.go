package user

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/punyasaya19/go-api-2/helper"
	"gorm.io/gorm"
)

type UserController struct {
	userService UserService
	// ctx         *gin.Context
}

// func to init
func NewUserController(con *gorm.DB) UserController {
	return UserController{
		userService: NewUserService(con),
		// ctx:         ctx,
	}
}

func (uc *UserController) Index(ctx *gin.Context) {
	var response helper.ResponseJson
	datas := uc.userService.GetAll()

	response.Status = true
	response.Message = "Get All Data"
	response.Data = datas
	response.Error = nil

	ctx.JSON(http.StatusOK, response)
}

func (uc *UserController) Show(ctx *gin.Context) {
	var response helper.ResponseJson
	var responseCode int

	// Get id update & convert to int
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		response.Status = false
		response.Message = "Invalid Request"
		response.Error = err
		response.Data = nil
		responseCode = http.StatusBadRequest
	} else {
		// Get data
		data := uc.userService.GetById(reqId)

		response.Status = true
		response.Message = "Get Data By ID"
		response.Data = data
		response.Error = nil
		responseCode = http.StatusOK
	}

	ctx.JSON(responseCode, response)
}

func (uc *UserController) GetByUsername(ctx *gin.Context) {
	// var response helper.ResponseJson
	// var responseCode int = http.StatusOK

	// // Get request username

}

func (uc *UserController) Create(ctx *gin.Context) {
	var response helper.ResponseJson
	var responseCode int = http.StatusOK

	result, err := uc.userService.Create(ctx)
	if err != nil {
		response.Status = false
		response.Message = "Something Error"
		response.Error = err
		response.Data = nil
		responseCode = http.StatusInternalServerError
	} else {
		response.Status = true
		response.Message = "Success Create Data!"
		response.Data = result
		response.Error = nil
	}
	ctx.JSON(responseCode, response)
}

func (uc *UserController) Update(ctx *gin.Context) {
	var response helper.ResponseJson
	var responseCode int = http.StatusOK

	result, err := uc.userService.Update(ctx)
	if err != nil {
		response.Status = false
		response.Message = "Something Error"
		response.Error = err
		response.Data = nil
		responseCode = http.StatusInternalServerError
	} else {
		response.Status = true
		response.Message = "Success Update Data!"
		response.Data = result
		response.Error = nil
	}
	ctx.JSON(responseCode, response)
}

func (uc *UserController) Delete(ctx *gin.Context) {
	var response helper.ResponseJson
	var responseCode int = http.StatusOK

	result, err := uc.userService.Delete(ctx)
	if err != nil {
		response.Status = false
		response.Message = "Something Error"
		response.Error = err
		response.Data = nil
		responseCode = http.StatusInternalServerError
	} else {
		response.Status = true
		response.Message = "Success Delete Data!"
		response.Data = result
		response.Error = nil
	}
	ctx.JSON(responseCode, response)
}
