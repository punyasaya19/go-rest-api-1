package user

import "time"

type User struct {
	ID        int64     `json:"id" gorm:"primaryKey;auto_increment:true;index"`
	Name      string    `json:"name" gorm:"varchar(255)"`
	Username  string    `json:"username" gorm:"varchar(255)"`
	Password  string    `json:"password" gorm:"varchar(255)"`
	CreatedAT time.Time `json:"created_at"`
	UpdatedAT time.Time `json:"updated_at"`
}
