package user

type UserCreateInput struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserUpdateInput struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	Password string `json:"password"`
}
