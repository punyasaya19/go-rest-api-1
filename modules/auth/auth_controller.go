package auth

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/punyasaya19/go-api-2/config"
	"gitlab.com/punyasaya19/go-api-2/helper"
	"gorm.io/gorm"
)

type AuthController struct {
	authService AuthService
}

func NewAuthController(con *gorm.DB) AuthController {
	return AuthController{authService: NewAuthService(con)}
}

func (controller *AuthController) Login(ctx *gin.Context) {
	var authLoginInput AuthLoginInput
	err := ctx.ShouldBind(&authLoginInput)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Failed Parseing JSON Request",
			Data:    nil,
			Error:   err,
		})
		return
	}

	// Validation input
	validate := validator.New()
	err = validate.Struct(&authLoginInput)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.ResponseJson{
			Status:  false,
			Message: "Validation Error",
			Data:    nil,
			Error:   err.(validator.ValidationErrors),
		})
		return
	}

	// Cek username & password
	statusCode, user, err := controller.authService.Login(&authLoginInput, ctx)
	if err != nil {
		fmt.Println(err)
		ctx.JSON(statusCode, helper.ResponseJson{
			Status:  false,
			Message: "Validation Error",
			Data:    nil,
			Error:   err.Error(),
		})
		return
	}

	// Jika berhasil set session

	ctx.JSON(statusCode, helper.ResponseJson{
		Status:  true,
		Message: "Success Login",
		Data:    user,
		Error:   nil,
	})
}

func (controller *AuthController) Logout(ctx *gin.Context) {
	session, _ := config.Store.Get(ctx.Request, config.SESSION_ID)

	// Delete session
	session.Options.MaxAge = -1

	session.Save(ctx.Request, ctx.Writer)
	ctx.JSON(http.StatusOK, helper.ResponseJson{
		Status:  true,
		Message: "Success Logout",
		Data:    nil,
		Error:   nil,
	})
}
