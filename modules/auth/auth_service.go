package auth

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/punyasaya19/go-api-2/config"
	"gitlab.com/punyasaya19/go-api-2/modules/user"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type AuthServiceImpl struct {
	userService user.UserService
}

// Login implements AuthService.
func (service *AuthServiceImpl) Login(authLoginInput *AuthLoginInput, ctx *gin.Context) (statusCode int, user user.User, err error) {
	sts, user := service.userService.GetByUsername(authLoginInput.Username)

	if sts != 1 {
		fmt.Println("username tdk ditemukan")
		// User tdk ditemukan
		errMsg := errors.New("username tidak ditemukan")
		return http.StatusBadRequest, user, errMsg
	}

	// Cek password
	errPassword := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(authLoginInput.Password))
	if errPassword != nil {
		// Password Salah
		return http.StatusBadRequest, user, errors.New("password salah")
	}

	// Jika berhasil
	// Set Session
	session, _ := config.Store.Get(ctx.Request, config.SESSION_ID)
	session.Values["isLogin"] = true
	session.Values["username"] = user.Username
	session.Values["name"] = user.Name
	session.Save(ctx.Request, ctx.Writer)
	return http.StatusOK, user, nil
}

// Logout implements AuthService.
func (service *AuthServiceImpl) Logout() bool {
	panic("unimplemented")
}

// Interface
type AuthService interface {
	Login(authLoginInput *AuthLoginInput, ctx *gin.Context) (statusCode int, user user.User, err error)
	Logout() bool
}

// Func init
func NewAuthService(con *gorm.DB) AuthService {
	return &AuthServiceImpl{userService: user.NewUserService(con)}
}
