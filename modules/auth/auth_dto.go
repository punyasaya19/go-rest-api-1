package auth

type AuthLoginInput struct {
	Username string `json:"username" validation:"required"`
	Password string `json:"password" validation:"required"`
}
