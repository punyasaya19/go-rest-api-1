package helper

type ResponseJson struct {
	Status  bool
	Message string
	Error   any
	Data    any
}
