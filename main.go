package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/punyasaya19/go-api-2/config"
	"gitlab.com/punyasaya19/go-api-2/modules/auth"
	"gitlab.com/punyasaya19/go-api-2/modules/category"
	"gitlab.com/punyasaya19/go-api-2/modules/user"
)

func MyMdlware() gin.HandlerFunc {
	return func(c *gin.Context) {
		session, _ := config.Store.Get(c.Request, config.SESSION_ID)
		if len(session.Values) == 0 {
			fmt.Println("Tdk ada session")
			c.JSON(http.StatusForbidden, map[string]any{
				"Status":  false,
				"Message": "Anautorize",
				"Data":    nil,
				"Error":   "Please Login First",
			})
			c.Abort()
			return
		} else {
			fmt.Println("Ada session")
			c.Next()
		}

	}
}

func main() {
	// Create Connection
	db := config.ConnectDB()

	// Init Controllers
	userController := user.NewUserController(db)
	categoryController := category.NewCategoryController(db)
	authController := auth.NewAuthController(db)

	// Routing
	r := gin.Default()

	// Login
	r.POST("/login", authController.Login)
	r.POST("/logout", authController.Logout)

	// User
	userRoute := r.Group("/users")
	userRoute.Use(MyMdlware())
	{
		userRoute.GET("/", userController.Index)
		userRoute.GET("/:id", userController.Show)
		userRoute.POST("/", userController.Create)
		userRoute.PUT("/:id", userController.Update)
		userRoute.DELETE("/:id", userController.Delete)
	}

	// Category
	categoryRoute := r.Group("/category")
	categoryRoute.Use(MyMdlware())
	{
		categoryRoute.GET("/", categoryController.Index)
		categoryRoute.GET("/:id", categoryController.Show)
		categoryRoute.POST("/", categoryController.Create)
		categoryRoute.PUT("/:id", categoryController.Update)
		categoryRoute.DELETE("/:id", categoryController.Delete)
	}

	fmt.Println("Server berjalan di http://locahost:3000")
	r.Run(":3000")
}
