package config

import (
	"gitlab.com/punyasaya19/go-api-2/modules/category"
	"gitlab.com/punyasaya19/go-api-2/modules/product"
	"gitlab.com/punyasaya19/go-api-2/modules/user"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// DB config
const DB_DRIVER = "mysql"
const DB_HOST = "localhost"
const DB_PORT = "3306"
const DB_USER = "roots"
const DB_PASS = "roots"
const DB_NAME = "go_api_2"

func ConnectDB() *gorm.DB {
	dsn := DB_USER + ":" + DB_PASS + "@tcp(" + DB_HOST + ":" + DB_PORT + ")/" + DB_NAME + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	// Migration
	err = db.AutoMigrate(&user.User{}, &category.Category{}, &product.Product{})
	if err != nil {
		panic(err)
	}

	return db
}
